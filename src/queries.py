#-----------------------------MONGO---------------------------------------#


#-----------------------------BUS ODF-KANDI-------------------------------#

def add_odf2kandi(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.odf2kandi
        add_timings.insert({'time': tm})


def next_odf2kandi(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.odf2kandi
        results = timings.find({ "time": { "$gt": tm} }).sort( [( "time", 1 )])
        return results


def add_kandi2odf(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.kandi2odf
        add_timings.insert({'time': tm})


def next_kandi2odf(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.kandi2odf
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_odf2kandi_weekend(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.odf2kandi_weekend
        add_timings.insert({'time': tm})


def next_odf2kandi_weekend(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.odf2kandi_weekend
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_kandi2odf_weekend(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.kandi2odf_weekend
        add_timings.insert({'time': tm})


def next_kandi2odf_weekend(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.kandi2odf_weekend
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_kandi2odf_summer(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.kandi2odf_summer
        add_timings.insert({'time': tm})


def next_kandi2odf_summer(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.kandi2odf_summer
        add_timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])


def add_odf2kandi_summer(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.odf2kandi_summer
        add_timings.insert({'time': tm})


def next_odf2kandi_summer(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.odf2kandi_summer
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


#--------------------------------------------------#


#----------------CAB-HOSTEL-LAB------------------------#


def add_hostel2lab(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.hostel2lab
        add_timings.insert({'time': tm})


def next_hostel2lab(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.hostel2lab
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_lab2hostel(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.lab2hostel
        add_timings.insert({'time': tm})


def next_lab2hostel(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.lab2hostel
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_hostel2lab_weekend(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.hostel2lab_weekend
        add_timings.insert({'time': tm})


def next_hostel2lab_weekend(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.hostel2lab_weekend
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_lab2hostel_weekend(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.lab2hostel_weekend
        add_timings.insert({'time': tm})


def next_lab2hostel_weekend(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.lab2hostel_weekend
        results = timings.find({'time': { "$gt": tm}}).sort( [( "time", 1 )])
        return results

#-------------------------------------------------#


#----------------BUS-HOSTEL-INSTITUTE-----------------#

def add_odf2insti(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.odf2insti
        add_timings.insert({'time': tm})


def next_odf2insti(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.odf2insti
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results


def add_insti2odf(mongo,ctx,tm):
    with ctx:
        add_timings = mongo.db.insti2odf
        add_timings.insert({'time': tm})


def next_insti2odf(mongo,ctx,tm):
    with ctx:
        timings = mongo.db.insti2odf
        results = timings.find({'time': {"$gt": tm}}).sort( [( "time", 1 )])
        return results

#-------------------------------------------------#


#------------------MESS---------------------------#

def add_mess_menu(mongo,ctx,day, menu):
    with ctx:
        
        add_timings = mongo.db.mess
        add_timings.insert({'day': day, 'time_of_day':'breakfast' ,'menu': menu[0]})
        add_timings.insert({'day': day, 'time_of_day':'lunch', 'menu': menu[1]})
        add_timings.insert({'day': day, 'time_of_day':'snacks', 'menu':menu[2]})
        add_timings.insert({'day': day, 'time_of_day':'dinner','menu': menu[3]})


def get_mess_menu(mongo,ctx,day,time_of_day):
    with ctx:
        timings = mongo.db.mess
        results = timings.find({'day': day,'time_of_day':time_of_day})
        return results


#-------------------------------------------------#



#----------------Lost and Found -------------------#


def lost_item(mongo,ctx,sender_id,item,day):
    with ctx:
        lost_items = mongo.db.lost_items
        lost_items.insert({'lost_member':sender_id,'item':item,'day':day})
        return    

def search_lost(mongo,ctx,item):
    with ctx:
        lost_item  = mongo.db.lost_items
        return lost_item.find({"item":item})

def found_item(mongo,ctx,sender_id,item,picture,day):
    with ctx:
        found_item = mongo.db.found_item
        found_item.insert({'item':item,'found_member':sender_id,'picture':picture,'day':day})
        return 
def search_found(mongo,ctx,item,day):
    with ctx:
         found_item = mongo.db.found_item.find({"item":item,'day':day})
         return found_item

def remove_3days_before_days(mongo,ctx,day):
    with ctx:
        mongo.db.lost_items.remove({'day':day})
        mongo.db.found_item.remove({'day':day})
        return 

    
#--------------------------------------------------#



#------------Jokes----------------------#
def add_jokes(mongo,ctx,joke):
    with ctx:
            add_jks = mongo.db.jokes
            add_jks.insert({"jokes":joke})

def get_joke(mongo,ctx):
    with ctx:
        joke = mongo.db.jokes.aggregate([{ "$sample": { "size":1} }])            
        return joke
#---------------------------------------#


def alloweduser(mongo,ctx,sender):
	with ctx:
		users = mongo.db.users
		return users.find({"user_id":sender})

def adduser(mongo,ctx,sender,email):
	with ctx:
		users = mongo.db.users
		users.insert({"user_id":sender,"email":email})
		hashcode  = mongo.db.codes
		hashcode.remove({"user_id":sender})
		return           

def sendmailget(mongo,ctx,sender):
	with ctx:
		hashcode = mongo.db.codes
		res = hashcode.find({"user_id":sender}).sort([("$natural",-1)]).limit(1)
		return res
		
def sendmailadd(mongo,ctx,sender,hashcode):
	with ctx:
		hashcodes  = mongo.db.codes
		hashcodes.insert({"user_id":sender,"hashcode":hashcode})



#--------------------courses-------------------------------------#

def addClasses(mongo,ctx,batch,courses):
    with ctx:
        print courses
        courses = courses[1:]
        coursesDB = mongo.db.courses
        slotsDB = mongo.db.slots
        for each in courses:
            slots = each[5]
            slots = slots.split('|')
            coursesDB.insert({"batch":batch,"course_id":each[0],"course_name":each[1],"semester":each[2],"from_segment":each[3],"to_segment":each[4],"room_no":each[6]})
            slotsDB.insert({"course_id":each[0],"slots":slots})


def getClasses(mongo,ctx,batch):
    with ctx:
        coursesDB = mongo.db.courses
        return coursesDB.find({"batch":batch})


def addUserCourses(mongo,ctx,sender,courses):
    with ctx:
        userCourses = mongo.db.usercourses
        userCourses.insert({"user_id":sender,"courses":courses})  


def getUserClasses(mongo,ctx,sender):
    with ctx:
           userCourses = mongo.db.usercourses
           return userCourses.find({"user_id":sender})

def getClassesSlots(mongo,ctx,course_id):
    with ctx:
        slots = mongo.db.slots 
        return slots.find({"course_id":course_id})         
def addUserEmail(mongo,ctx,sender,email):
    with ctx:
        emails = mongo.db.emails
        emails.insert({"user_id":sender,"email":email})

def getUserEmail(mongo,ctx,sender):
    with ctx:
                emails = mongo.db.emails
                return emails.find({"user_id":sender})

#----------------------------------------------------------------------#
