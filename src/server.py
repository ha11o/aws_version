#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import calendar
import datetime
import json
import logging
import os
import pprint
import sys
import urllib
import hashlib

from werkzeug.contrib.fixers import ProxyFix
import requests

import apiAI
import createDB
import getdata
import messenger
import queries
from apscheduler.schedulers.blocking import BlockingScheduler
from cachetools import LRUCache
from cachetools import TTLCache
from flask import Flask, request, session
from flask_pymongo import PyMongo
import time
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from flask_mail import Mail, Message
try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            os.pardir,
            os.pardir
        )
    )

    import apiai


logging.basicConfig(filename='logs.log',level=logging.INFO)
logger = logging.getLogger(__name__)

cache = TTLCache(maxsize=100,ttl=60, timer=time.time, missing=None, getsizeof=None)

app = Flask(__name__)
app.secret_key = '\xf1~E\x8adE2g\xe5\x00m\x045\xff_\x95h\xe7\xdd\x12[4N\x9a'
app.wsgi_app = ProxyFix(app.wsgi_app)
app.debug = True
# app.config['MONGO_DBNAME'] = 'iithassist'
# app.config['MONGO_URI'] = 'mongodb://ha11o:ha11o@ds129333.mlab.com:29333/iithassist'

#for mail authntication
mail = Mail(app)
app.config.update(
	MAIL_SERVER = 'smtp.gmail.com',
	MAIL_PORT = 465,
	MAIL_USE_SSL = True,
	MAIL_USERNAME = 'mail.for.mysticfalls@gmail.com',
	MAIL_PASSWORD = 'mtdwtfhmjqhvnqxh',
)

mail = Mail(app)

ctx = app.app_context()
ctx.push()

mongo = PyMongo(app)


#-----------------------CRON JOBS--------------------------------------#


def refreash_db():
    try:
	    createDB.CreateDB(mongo,ctx)
    except:
        logger.info('Error occured while updating the DB')
scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
    func=refreash_db,
    trigger=IntervalTrigger(hours=6),
    id='printing_job',
    name='sheduler for sheets',
    replace_existing=True)
atexit.register(lambda: scheduler.shutdown())
#-----------------------------------------------------------------------#


#--------------------------APIai----------------------------------------#
def parse_apiai(mssg):
    result = mssg.get('result')
    if result:
        parameters = result.get("parameters")
        fulfillment = result.get('fulfillment')
        action = result.get('action')
        return action, parameters, fulfillment.get("speech")
    else:
        return None, None, None


#----------------------converter timestamp-----------------------------#

def get_day(tmstamp):
    """
       convert the timestamp to day
    """
    tmstamp = str(tmstamp)
    tmstamp_stripped = tmstamp[:-3]
    dt = datetime.datetime.fromtimestamp(int(tmstamp_stripped)).today()
    return calendar.day_name[dt.weekday()]

def get_nextday(tmstamp):
    """
       convert the timestamp to day
    """
    tmstamp = str(tmstamp)
    tmstamp_stripped = tmstamp[:-3]
    dt = datetime.datetime.fromtimestamp(int(tmstamp_stripped)).today()
    return calendar.day_name[(dt.weekday()+1)%7]

def get_t(tmstamp):
    """
        convert timestamp to date and time
    """
    tmstamp = str(tmstamp)
    tmstamp_stripped = tmstamp[:-3]
    dt = datetime.datetime.fromtimestamp(
        int(tmstamp_stripped)).strftime('%Y-%m-%d %H:%M:%S')
    print(dt.split()[1])
    return dt.split()[1]

#-----------------------------------------------------------------------#

# send messge to messenger

def format_time(nxt):
    return  str(nxt/100).zfill(2)+str(':')+ str(nxt%100).zfill(2)

def SendThis(sender, nxt, bot_mssg):
    try:
        if nxt:
            temp =  []
            #print(nxt[0],'Hey there')
            cnt = 0
            for i in nxt:
                print (i.get("time"))
                temp.append(i.get("time"))
            temp = list(set(temp))
            temp.sort()
            temp = temp[:5]
            for i in temp:
                bot_mssg+= format_time(i) + ' | '
            if len(temp)==0:
                messenger.send_message(sender,'Nothing found at this time. Sry :(')
            else:
                messenger.send_message(sender, bot_mssg)

    except:
        messenger.send_message(sender, 'Sorry can\'t find anything at this time\n')


def send_menu(sender, day, time_of_day):
    mess_menu = queries.get_mess_menu(mongo, ctx, day, time_of_day)
    for each in mess_menu:
        mm = list(each["menu"])
        print (mm)
        today_menu = ', '.join(mm)
        messenger.send_message(sender, today_menu.replace('\n',''))
        return


def process_direction(direction):
    change_direction = ''
    dir_len = direction.split()
    print (dir_len)
    if 'from' in dir_len:
        change_direction = dir_len[2]+' to '+ dir_len[0]
        return change_direction.replace('?','')
    else:
        return direction.replace('?','')



#----------------auth-------------------#
def isUserAuthed(sender):
	val = queries.alloweduser(mongo,ctx,sender)
	if val.count()==0:
		return  False
	else:
		return True

def addUser(sender):
	val = queries.adduser(mongo,ctx,sender)
	return


def sendmail_get(sender):
	val = queries.sendmailget(mongo,ctx,sender)
	if val.count()>0:
		return val[0]["hashcode"]

def sendmail_add(sender,hashcode):
	val = queries.sendmailadd(mongo,ctx,sender,hashcode)
	return
#-----------------------flask-------------------------------------------#



"""
	make sure tat we are using mongodb serverion greater than 3.4
    take care about cache ok?
    it will be a pain in the ass

"""



###########   COllect ID's    ##############
#ids_file = None
#if os.path.isfile('./ids.txt'):
#	print('file exists')
#	ids_file = open('ids.txt','a')
#else:
#	ids_file =  open('ids.txt','w')


@app.route("/", methods=['POST','GET'])
def ConnectMessenger():
        if request.args.get('hub.verify_token','') == 'hiiamha11o':
           return request.args.get('hub.challenge','')
        data = request.get_data()
        sender, mssg, time = messenger.messaging_events(data)
        if mssg == 'GET_STARTED_PAYLOAD':
                messenger.send_message(sender, 'Hi,Welcome ' + messenger.name(sender)
                               [0] + ' I am your assit I will help you with Mess Menu , Next bus and Lost and Found')
#	    messenger.send_message(sender,'first provide your email for accessing the bot ')
                return ''
        messenger.mssg_read(sender)
#	ids_file.write(str(sender)+'\n')
	
        """
	    #-------------------------AUTHENTICATION----------------------------------#
	    if not isUserAuthed(sender):
		    mssg_email = mssg.split('@')
		    if len(mssg_email)>1 and  mssg.split('@')[1] == 'iith.ac.in':
			    logger.info('Sending mail',mssg_email)
			    mssg_for_auth = Message(
				    		'Authentication code  for IITH Assistant',
					    	sender = 'mail.for.mysticfalls@gmail.com',
						    recipients = [mssg]
					    )
			    hashcode = str(hashlib.md5(sender).hexdigest())
			    hashcode  = 'ha11o:'+mssg+':'+hashcode
			    mssg_for_auth.body  = "Send this code to bot:\t\t" + hashcode
			    sendmail_add(sender,hashcode)
			    mail.send(mssg_for_auth)
			    messenger.send_message(sender,'Send auth code to mail please check it and enter the code')
		    elif len(mssg) > 1 and mssg.split(':')[0]=='ha11o':
			    print(sendmail_get(sender))
			    if sendmail_get(sender)==mssg:
				    addUser(sender)
                    queries.addUserEmail(mongo,ctx,sender,mssg.split(":")[1])
				    messenger.send_message(sender,'Yuppi! Authentication Done!')
			    else:
				    messenger.send_message(sender,'Wrong auth code paste correctly')
		    else:
                    messenger.send_message(sender,'Hey provide your iith email for autentication')
		    return ''
	"""
        # try:
        if sender:
            splitted_mssg = mssg.split(':::')
            if splitted_mssg[0]=='userbranch':
                temp_ls = []
                branch = splitted_mssg[1]
                for z in range(14,18):
                    temp_ls.append(branch+str(z)+'B')
                for z in range(16,18):
                    temp_ls.append(branch+str(z)+'M')    
                messenger.send_message_with_buttions(sender,'Which batch? '+messenger.name(sender)[0],"user_batch:::",temp_ls)
            elif splitted_mssg[0]=='user_batch':
                messenger.send_message_with_buttions(sender,"classes","hi:::",queries.getClasses(splitted_mssg[1]))
            elif (splitted_mssg)[0] == 'ha11o':
                local_mssg = splitted_mssg[2]
                vehicle = splitted_mssg[1]
                day = get_day(time)
                time_now = get_t(time)
                time_now = time_now.split(':')
                time_now = int(time_now[0]) * 100 + int(time_now[1])
                if local_mssg == u'hostel' and vehicle == 'cab':
                    bot_mssg = 'Hey '+messenger.name(sender)[0]+' next cab to lab timings'
                    if day == 'Saturday' or day == 'Sunday':
                        next_cab = queries.next_hostel2lab_weekend(
                            mongo, ctx, time_now)
                        SendThis(sender, next_cab, bot_mssg)
                        session.pop('intent', None)
                        return ''

                    else:
                        next_cab = queries.next_hostel2lab(
                            mongo, ctx, time_now)
                        SendThis(sender, next_cab, bot_mssg)
                        session.pop('intent', None)
                        return ''
                elif local_mssg == u'lab' and vehicle == 'cab':
                    bot_mssg = 'Hey '+messenger.name(sender)[0]+' next cab to hostel timings'
                    if day == 'Saturday' or day == 'Sunday':
                        next_cab = queries.next_lab2hostel_weekend(
                            mongo, ctx, time_now)
                        SendThis(sender, next_cab, bot_mssg)
                        session.pop('intent', None)
                        return ''

                    else:
                        next_cab = queries.next_lab2hostel(
                            mongo, ctx, time_now)
                        SendThis(sender, next_cab, bot_mssg)
                        session.pop('intent', None)
                        return ''
                else:
                    if local_mssg == 'odf':
                        messenger.send_message_with_buttions(
                            sender, 'To where ' + messenger.name(sender)[0] + '?', 'ha11o:::odf:::', ['kandi', 'institute'])
                    elif local_mssg == 'kandi':
                        if vehicle == 'odf':
                            if day=="Saturday" or day=="Sunday":
                                next_bus = queries.next_odf2kandi_weekend(mongo,ctx,time_now)
                            else:
                            	next_bus = queries.next_odf2kandi(
                                	mongo, ctx, time_now)
                            bot_mssg = 'hey,'+messenger.name(sender)[0]+' next bus to odf-kandi is on '
                            SendThis(sender, next_bus, bot_mssg)
                            session.pop('intent', None)
                        else:
                            if day=="Saturday" or day=="Sunday":
				                    next_bus = queries.next_kandi2odf_weekend(mongo, ctx, time_now)
                            else:
                                 next_bus = queries.next_kandi2odf(mongo,ctx,time_now)
                            bot_mssg = 'Hey,'+messenger.name(sender)[0]+' The next bus to kandi-odf is on '
                            SendThis(sender, next_bus, bot_mssg)
                            session.pop('intent', None)
                    elif local_mssg == 'institute':
                        if vehicle == 'odf':
                            next_bus = queries.next_odf2insti(
                                mongo, ctx, time_now)
                            bot_mssg = 'Hey ,'+messenger.name(sender)[0]+'  the next bus to odf-insti is on '
                            SendThis(sender, next_bus, bot_mssg)
                            session.pop('intent', None)
                        else:
                            bot_mssg = 'hey ,'+messenger.name(sender)[0]+'  the next bus to insti-odf is on '
                            next_bus = queries.next_insti2odf(
                                mongo, ctx, time_now)
                            SendThis(sender, next_bus, bot_mssg)
                            session.pop('intent', None)
                    return ''



            elif splitted_mssg[0]=='ifoundsomething':
                item = splitted_mssg[1]
                cache[sender] = ['found',item]
                messenger.send_message(sender,messenger.name(sender)[0]+' take a picture of the item and send?')
            elif splitted_mssg[0]=='menuquery':
                    if splitted_mssg[1]=='Mess Menu':
                        messenger.send_message_with_buttions(sender, 'Hey ' + messenger.name(
                            sender)[0] + ' which menu you want?', 'today ', ['breakfast', 'lunch','snacks', 'dinner'])
                    elif splitted_mssg[1]=='Next cab':
                                messenger.send_message_with_buttions(
                                sender, 'From where ' + messenger.name(sender)[0] + '?', 'ha11o:::cab:::', ['hostel', 'lab'])
                    elif splitted_mssg[1]=='Next bus':
                            messenger.send_message_with_buttions(sender, 'From where ' + messenger.name(
                                sender)[0] + '?', 'ha11o:::bus:::', ['odf', 'kandi', 'institute'])
                    elif splitted_mssg[1]=='Lost':
                        messenger.send_message_with_buttions(sender,messenger.name(sender)[0]+' tell me what you lost? ','ilostsomething:::',["mobile","money","earphones","wallet","keys","book","bag","laptop","hard disk","pen drive","others"])
                    elif splitted_mssg[1]=='Found':
                         messenger.send_message_with_buttions(sender,messenger.name(sender)[0]+' tell me what you found?','ifoundsomething:::',["mobile","money","earphones","wallet","keys","book","bag","laptop","hard disk","pen drive","others"])
                    else:
                        messenger.send_message(sender,'Can\'t help you buddy')

            elif splitted_mssg[0]=='thisisaimageha11o':
                print(cache)
                if sender in cache:
                    if cache[sender][0]=='found':
                        item = cache[sender][1]
                        see_lost_items = queries.search_lost(mongo,ctx,item)
                        for each in see_lost_items:
                            messenger.send_message(each["lost_member"],"Is this your item? Contact: "+messenger.name(sender)[0]+' '+messenger.name(sender)[1])
                            messenger.send_image(each["lost_member"],splitted_mssg[1])
                            logger.info('Send the user about the item')
			    #time.sleep(0.2)
                        queries.found_item(mongo,ctx,sender,item,splitted_mssg[1],get_day(time))
                        messenger.send_message(sender,'Thankyou for your help :) ! The owner will contact you :)')
                        cache[sender][0]='told_you'
                        print('changed',cache)
                        logger.info('Inserted found item into DB')
                    else:
                         messenger.send(sender,'Sorry timeout try again! (timeout is 60sec)')

            elif splitted_mssg[0]=='ilostsomething':
                item = splitted_mssg[1]
                day = get_day(time)
                var_ha11o= 0
                same_items_found = queries.search_found(mongo,ctx,item,day)
                if (same_items_found.count())>0:
                         messenger.send_message(sender,'Hey '+messenger.name(sender)[0]+ ' some of the same items found today')
                else:
                    var_ha11o = var_ha11o + 1
                for i in same_items_found:
                        messenger.send_message(sender,'If the item is your\'s then , Contact:'+messenger.name(i["found_member"])[0] +' '+messenger.name(i["found_member"])[1] )
                        messenger.send_image(sender,i["picture"])

                queries.lost_item(mongo,ctx,sender,item,get_day(time))
                logger.info('Inserted a lost request to db')
                if var_ha11o==1:
			        messenger.send_message(sender,'Sorry nothing found today :( I will inform you if someone found that item')

            else:
                response = apiAI.APIai(mssg)
                action, parameters, speech = parse_apiai(response)
                logger.info(action)
                logger.info(parameters)
                if action==u'classes_menu':
                    messenger.send_message_with_buttions(sender,"Hey what is your branch?","userbranch:::",["CS","EE","CE","MS","ME","EP","ES","CH"])
                elif action == u'give_the_menu':
                    messenger.send_message_with_buttions(sender,messenger.name(sender)[0]+'What you want me to do?','menuquery:::',['Mess Menu','Next bus','Next cab','Lost','Found'])
                elif action ==u'talk_with_alone_mf':
			        messenger.send_message(sender,speech)
                elif action == u'input.welcome':
                    messenger.send_message(sender, speech+' '+messenger.name(sender)[0]+'!')

                elif action == u'input.unknown':
                    messenger.send_message(sender, speech)
                elif action==u'found':
                    if not parameters.get("item"):
                        messenger.send_message_with_buttions(sender,messenger.name(sender)[0]+' tell me what you found?','ifoundsomething:::',["mobile","money","earphones","wallet","keys","book","bag","laptop","hard disk","pen drive","others"])
                    else:
                        messenger.send_message(sender,messenger.name(sender)[0]+' take a picture of the item and send?')
                        pass
                elif action==u'lost':
                    if not parameters.get("item"):
                        messenger.send_message_with_buttions(sender,messenger.name(sender)[0]+' tell me what you lost? ','ilostsomething:::',["mobile","money","earphones","wallet","keys","book","bag","laptop","hard disk","pen drive","others"])
                    else:
                        pass
                elif action == u'get_mess_menu':
                    day = get_day(time)
                    if parameters["messmenu"] == u'mess menu' or parameters["messmenu"]==u'mess' or parameters["messmenu"]==u'messmenu' or parameters["messmenu"]==u'today\'s mess menu':
                        if 'tomorrow' in mssg:
                            messenger.send_message_with_buttions(sender, 'Hey ' + messenger.name(
                                sender)[0] + ' which menu you want?', 'tomorrow ', ['breakfast', 'lunch','snacks', 'dinner'])
                        else:
                            messenger.send_message_with_buttions(sender, 'Hey ' + messenger.name(
                                sender)[0] + ' which menu you want?', 'today ', ['breakfast', 'lunch','snacks', 'dinner'])
                    else:
                        time_of_day = parameters["time_of_day"]
			print (mssg)
                        if 'tomorrow\'s' in mssg.lower() or 'tomorrow' in mssg.lower():
                            day = get_nextday(time)
                            print(day)
                        if len(time_of_day) != 0:
                            send_menu(sender, day, time_of_day)
                        else:
                            messenger.send_message(
                                sender, 'Sorry i did not find anything')
                        return ''
                elif action ==u'get_a_jokes_send_to_mf':
                    joke = queries.get_joke(mongo,ctx)
                    joke  = (list(joke))
                    messenger.send_message(sender,joke[0].get("jokes"))
                
                elif action == u'vehicle_next_timings_query':
                    bot_mssg = ''
                    day = get_day(time)
                    time_now = get_t(time)
                    time_now = time_now.split(':')
                    time_now = int(time_now[0]) * 100 + int(time_now[1])
                    pprint.pprint(parameters)

                    if len(parameters["direction"]) > 0:
                        direction = process_direction(parameters["direction"].lower())
                        logger.info(direction)
                        if direction == 'hostel to lab':

                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+'you can catch the next cab timings'
                                if day == 'Saturday' or day == 'Sunday':
                                    next_cab = queries.next_hostel2lab_weekend(
                                        mongo, ctx, time_now)
                                    SendThis(sender, next_cab, bot_mssg)
                                    return ''

                                else:
                                    next_cab = queries.next_hostel2lab(
                                        mongo, ctx, time_now)
                                    SendThis(sender, next_cab, bot_mssg)
                                    return ''

                        elif direction == 'lab to hostel':

                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'Hey '+messenger.name(sender)[0]+' you can catch the next cab timings '
                                if day == 'Saturday' or day == 'Sunday':
                                    next_cab = queries.next_lab2hostel_weekend(
                                        mongo, ctx, time_now)
                                    SendThis(sender, next_cab, bot_mssg)
                                    return ''

                                else:
                                    next_cab = queries.next_lab2hostel(
                                        mongo, ctx, time_now)
                                    SendThis(sender, next_cab, bot_mssg)
                                    return ''

                            if parameters["vehicle"][0].lower() == 'bus':
                                next_busses = queries.next_odf2kandi(
                                    mongo, ctx, time_now - 25)
                                bot_mssg = 'Hey '+messenger.name(sender)[0]+' the next bus from odf will be arriving at '
                                SendThis(sender, next_busses, bot_mssg)
                                return ''

                        elif direction == 'kandi to odf':

                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'Hey,Sorry there are no cabs but there are busses and the next bus timing is '
                                if day == "Saturday" or day == "Sunday":
                                    next_bus = queries.next_kandi2odf_weekend(mongo, ctx, time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                                else:
                                    next_bus = queries.next_kandi2odf(mongo,ctx,time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                                return ''

                            if parameters["vehicle"][0].lower() == 'bus':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+' next buses timing: '
                                if day=="Saturday" or day=="Sunday":
                                    next_bus = queries.next_kandi2odf_weekend(mongo, ctx, time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                                else:
                                    next_bus = queries.next_kandi2odf(mongo,ctx,time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                                return ''

                        elif direction == 'odf to kandi':

                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'hey,Sorry there are no cabs but there are busses and the next bus timing is '
                                if day=="Saturday" or day=="Sunday":
                                    next_bus=queries.next_odf2kandi_weekend(mongo,ctx,time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                                else:
                                    next_bus = queries.next_odf2kandi(
                                        mongo, ctx, time_now)
                                    SendThis(sender, next_bus, bot_mssg)
                            	return ''

                            if parameters["vehicle"][0].lower() == 'bus':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+' next buses timings from odf to kandi:'
                                if day=="Saturday" or day=="Sunday":
                                            next_bus = queries.next_odf2kandi_weekend(mongo,ctx,time_now)
                                            SendThis(sender, next_bus, bot_mssg)
                                else:
                                    	next_bus = queries.next_odf2kandi(
                                    		mongo, ctx, time_now)
                                        SendThis(sender, next_bus, bot_mssg)
                                return ''

                        elif direction == 'odf to institute':
                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'Hey, Sorry there are no cabs but there are busses and the next bus timing is '
                                next_bus = queries.next_odf2insti(
                                    mongo, ctx, time_now)
                                SendThis(sender, next_bus, bot_mssg)
                                return ''
                            if parameters["vehicle"][0].lower() == 'bus':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+ ' next buses from odf to insti: '
                                next_bus = queries.next_odf2insti(
                                    mongo, ctx, time_now)
                                SendThis(sender, next_bus, bot_mssg)
                                return ''

                        elif direction == 'institute to odf':
                            if parameters["vehicle"][0].lower() == 'cab':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+'Sorry there are no cabs but there are busses and the next bus timing is '
                                next_bus = queries.next_insti2odf(
                                    mongo, ctx, time_now)
                                SendThis(sender, next_bus, bot_mssg)
                                return ''
                            if parameters["vehicle"][0].lower() == 'bus':
                                bot_mssg = 'Hey, '+messenger.name(sender)[0]+' next buses from institue to odf :'
                                next_bus = queries.next_insti2odf(
                                    mongo, ctx, time_now)
                                SendThis(sender, next_bus, bot_mssg)
                                return ''

                    else:
                        if parameters["vehicle"][0] == 'cab':
                            messenger.send_message_with_buttions(
                                sender, 'From where ' + messenger.name(sender)[0] + '?', 'ha11o:::cab:::', ['hostel', 'lab'])
                        else:
                            messenger.send_message_with_buttions(sender, 'From where ' + messenger.name(
                                sender)[0] + '?', 'ha11o:::bus:::', ['odf', 'kandi', 'institute'])
                else:
                    messenger.send_message(sender, speech)
<<<<<<< HEAD

          return ''
        except:
        	messenger.send_message(sender, ' Sorry cannot handle that')
          	return ''

#-----------------------------------------------------------------------#i
createDB.CreateDB(mongo, ctx)
=======
        return ''
        # except:
        # 	messenger.send_message(sender, 'I cannot process this Sorry for that :( ?')
        #   	return ''

#-----------------------------------------------------------------------#

@app.route("/url/<batch>",methods=['POST','GET'])
def sendCourses(batch):
    courses = queries.getClasses(mongo,ctx,batch)
    for each in courses:
        pass
    return '<p>Hi</p>'    
@app.route("/fallback",methods=['POST','GET'])
def fallBack():
    return '<p>Erro therer</p>'

# createDB.CreateDB(mongo, ctx)
# messenger.SetGetStarted()

>>>>>>> 02ee3e3d06a6ce896e12ff1226c18cd731f82b88
if __name__ == "__main__":
    messenger.SetGetStarted()
    app.run(threaded=True,use_reloader=False,port=5002)
