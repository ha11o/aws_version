#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import json
import os
import pprint
import sys
import urllib

import requests
import logging 
import getdata
from apscheduler.schedulers.blocking import BlockingScheduler
from flask import Flask, request
from flask_pymongo import PyMongo
import apiAI
import messenger
import getdata

try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            os.pardir,
            os.pardir
        )
    )

    import apiai

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

CLIENT_ACCESS_TOKEN = 'c458c99006b1442aac1b15155e6366c9'

#-----------------------------API.ai---------------------------------------#


def APIai(user_message):
    logger.info('Request send to API.ai')
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)
    request = ai.text_request()
    request.query = user_message
    response = json.loads(request.getresponse().read())
    result = response['result']
    action = result.get('action')
    actionIncomplete = result.get('actionIncomplete', False)
    logger.info('Received responce from API.ai')
    return response


#-------------------------------------------------------------------------#