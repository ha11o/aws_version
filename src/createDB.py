#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import json
import os
import pprint
import sys
import urllib
import datetime
import requests
import calendar

import  logging


import getdata
from apscheduler.schedulers.blocking import BlockingScheduler
from flask import Flask, request
from flask_pymongo import PyMongo
import apiAI
import messenger
import getdata
import queries

try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            os.pardir,
            os.pardir
        )
    )

    import apiai

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)



def yesterday():
    dt = datetime.date.fromordinal(datetime.date.today().toordinal()-1)
    logger.info('Removing '+calendar.day_name[dt.weekday()]+' items')
    return calendar.day_name[dt.weekday()]



def ClearDB(mongo,ctx):
    mongo.db.kandi2odf_weekend.drop()
    mongo.db.odf2kandi_weekend.drop()
    mongo.db.kandi2odf.drop()
    mongo.db.lab2hostel_weekend.drop()
    mongo.db.hostel2lab_weekend.drop()
    mongo.db.odf2kandi.drop()
    mongo.db.hostel2lab.drop()
    mongo.db.mess.drop()
    mongo.db.insti2odf.drop()
    mongo.db.odf2insti.drop()
    mongo.db.lab2hostel.drop()
    mongo.db.hostel2lab.drop()
    mongo.db.odf2insti_weekend.drop()
    mongo.db.insti2odf_weekend.drop()
    mongo.db.jokes.drop()
    mongo.db.found_items.remove({"day":yesterday()})
    mongo.db.lost_items.remove({"day":yesterday()})
    return


def ChangeFormat(tm):
    split_time = tm.split(':')
    if len(split_time) < 2:
        return None
    else:
        get_first_2 = split_time[1][0:2]
        return (int(split_time[0]) * 100 + int(get_first_2))


def CreateDB(mongo,ctx):

    with ctx:
	logger.info('Started clearing the DB')
    # empty the whole records
    	ClearDB(mongo,ctx)
    	logger.info('Done clearning the DB')

    	o_k_weekend_values, o_k_values, o_i_values, cab_values, cab_weekend_values, mess_values = getdata.get_timings()
    	logger.info('started creating the DB')
   	 # week end shedule
        try:
    	    for i, each in enumerate(o_k_weekend_values):
        	    if each[0].lower() == u'ODF-KANDI'.lower():
	            	for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_odf2kandi_weekend(mongo,ctx,InsertThis)
        	    else:
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_kandi2odf_weekend(mongo,ctx,InsertThis)
        except:
            logger.info('Error inserting odf-kandi shedule weekend')
    # odf to kandi weekdays
        try:
    	    for i, each in enumerate(o_k_values):
        	    if each[0].lower() == u'ODF-KANDI'.lower():
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_odf2kandi(mongo,ctx,InsertThis)
        	    else:
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_kandi2odf(mongo,ctx,InsertThis)
        except:
            logger.info('Error inserting odf-kandi weekday')
    # odf hostel to odf insti
        try:
    	    for i, each in enumerate(o_i_values):
        	    if each[0].lower() == u'Hostel to Institute:'.lower():
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_odf2insti(mongo,ctx,InsertThis)
       	 	    else:
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
               			queries.add_insti2odf(mongo,ctx,InsertThis)
        except:
            logger.info('Error inserting hostal2insti')
    # kandi hostel to labi
        try:
    	    for i, each in enumerate(cab_values):
                if each[0].lower() == u'HOSTEL TO LAB:'.lower():
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_hostel2lab(mongo,ctx,InsertThis)
                else:
            		for j in range(1, len(each)):
                		InsertThis = ChangeFormat(each[j])
                		queries.add_lab2hostel(mongo,ctx,InsertThis)
        except:
            logger.info('Error inserting cab shedule')
    # kandi hostel to lab weekendi
        try:
    	    for i, each in enumerate(cab_weekend_values):
                if each[0].lower()=='hostel to lab':
                    for k in each:
                        print(k,ChangeFormat(k))
                        InsertThis = ChangeFormat(k)
                        if InsertThis:
                            queries.add_hostel2lab_weekend(mongo,ctx,InsertThis)
                else:
                    for k in each:
                        print(k,ChangeFormat(k))
                        InsertThis = ChangeFormat(k)
                        if InsertThis:
                            queries.add_lab2hostel_weekend(mongo,ctx,InsertThis)
        except:
            logger.info('Error inserting cab weekend shedule')
    	jokes_file  = open('jokes.txt','r')
    	jokes = jokes_file.readlines()
    	for each_joke in jokes:
        	queries.add_jokes(mongo,ctx,each_joke)

    	for i in range(0,len(mess_values)):
       		if len(mess_values[i])>=2:
        		  queries.add_mess_menu(mongo,ctx,mess_values[i][0],mess_values[i][1:])


        branches = ["CS"]#,"EE","CE","MS","ME","EP","ES","CH"] 

        for each in branches:
            for i in range(14,15):
                batch = each+str(i)+'B'
                courses = getdata.get_courses(batch)
                queries.addClasses(mongo,ctx,batch,courses)


    	logger.info('Done inserting into DB')

#--------------------------------------------------#
