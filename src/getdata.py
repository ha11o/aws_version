from __future__ import print_function
import httplib2
import os
import logger

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'

def get_credentials():
    credential_dir = '.credentials'
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def get_timings():

        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
        service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

        spreadsheetId =  '1-8tZ_gUDZ4D_sGrfmals6FqAodQ-aP-ZoX2WuR1dv_4'
        rangeName = 'ODF-KANDI Weekend SCHEDULE!A3:G12'
        result = service.spreadsheets().values().get(spreadsheetId=spreadsheetId , range = rangeName).execute()
        o_k_weekend_values = result.get('values',[])

        rangeName = 'A3:L12'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId, range=rangeName).execute()
        o_k_values = result.get('values', [])

        rangeName = 'ODF HOSTEL-INSTI SCHEDULE!A4:D'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId, range=rangeName).execute()
        o_i_values = result.get('values', [])

        rangeName = 'Cab Schedule_Weekdays!A3:H20'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId, range=rangeName).execute()
        cab_values = result.get('values', [])

        rangeName = 'Cab weekends!A3:E24'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId, range=rangeName).execute()
        cab_weekend_values = result.get('values', [])

        spreadsheetId_mess =  '128lCdRMBezEY9DwKr1v1SI6gt-WR036UExJnaarD01E'
        rangeName = 'MENU!A4:K10'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId_mess, range=rangeName).execute()
        values = result.get('values', [])

        mess_values = []
        for row in values:
            days     =  [row[0]]
            breakfast=  [row[1]]
            lunch    =  [row[2]+ ', ' +row[3] + ', '+row[4]]
            snacks   =  [row[5] +', '+ row[6]]
            dinner   =  [row[7] +', '+row[8] + ', '+ row[9] + ', '+row[10]]
            mess_values.append([days,breakfast,lunch,snacks,dinner])

        return o_k_weekend_values,o_k_values,o_i_values,cab_values,cab_weekend_values,mess_values



#just update the breanches id here 
branchesId = {}
branchesId["CS"] = '1jYk5KtaHOKQrsKwQrafNNrijZizGE2WxEWfj2mhlWrs'
branchesId["ES"] = ''
branchesId["EP"] = ''
branchesId["EE"] = ''
branchesId["MS"] = ''
branchesId["ME"] = ''
branchesId["CE"] = ''
branchesId["CH"] = ''


def get_courses(batch):
    
        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
        service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

        spreadsheetId  = branchesId[batch[0:2]]
        rangeName = str(batch)+'!A1:G25'
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheetId, range=rangeName).execute()
        values = result.get('values', [])
        return values

        