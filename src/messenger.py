#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import json
import os
import pprint
import sys
import urllib
import logging
import requests

import getdata
from apscheduler.schedulers.blocking import BlockingScheduler
from flask import Flask, request
from flask_pymongo import PyMongo
import apiAI
import messenger
import getdata
import logger
try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            os.pardir,
            os.pardir
        )
    )

    import apiai

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

token = 'EAAGnwa4Jjq0BAH9Q6XaHCFE86I6jUeiFvsQkhld8207DtDzC2LUCc6xC1jvg24xPEdFZCgue7eH9bEKXASw6mYzO5eUUetmdOrZCovt7ALf7pd4SUW4D5M7TaPa4uPWkJNuRecth8EAntP9fKoZBjXX1q2J51GZBh7gybbfxSAZDZD'
token = 'EAAGvah2xWUEBAE6CwPstY6rBovs7VNHUe5BEZC2P5POgFGozijwzrEh4q4BhOhwF1V6NWwZAxsShCo2SfXl6BsLihZCxZBq3PXfV7PrCh0j4hAQfqplq8Q2mFZBqsrjMu9iyNY60KB8j73NPikCZAy0NgXLPM7W3SUTD8kyZCZA6UgZDZD'

#------------------------------MESSENGER--------------------------------#

def send_message(recipient, message):
    r = requests.post("https://graph.facebook.com/v2.10/me/messages",
                      params={"access_token": token},
                      data=json.dumps({
                          "recipient": {"id": recipient},
                          "message": {"text": message}
                      }),
                      headers={'Content-type': 'application/json'})
    print(r.text)


def send_image(recipient, url):
    headers = {
        'Content-Type': 'application/json',
    }
    params = (
        ('access_token', token),
    )

    data = json.dumps({
        "recipient": {
            "id": recipient
        },
        "message": {
            "attachment": {
                "type": "image",
                "payload": {
                    "url": url
                }
            }
        }
    }
    )

    r = requests.post('https://graph.facebook.com/v2.10/me/messages',
                      headers=headers, params=params, data=data)
    return (r.text)


def send_message_with_url_buttions(receiver_id,message,url,buttions):
    create_buttions = []
    for each in buttions:
        create_buttions.append(
            {
                "type":"web_url",
                "url":str(url+"url/"+each),
                "title":each,
                "webview_height_ratio": "full",
                "messenger_extensions": True,  
                "fallback_url": url+"fallback"

            }
        )
    print (create_buttions)    
    headers = {'Content-Type': 'application/json', }
    params = (('access_token', token),)    
    data = json.dumps(
        {"recipient": {
            "id": receiver_id
        },
            "message": {
                  "attachment":{
                        "type":"template",
                        "payload":{  
                                    "template_type":"button",
                                     "text":"What do you want to do next?",
                                    "buttons":create_buttions
                        }
                  }
            } 
                            
        })  
    r = requests.post('https://graph.facebook.com/v2.10/me/messages',
                      headers=headers, params=params, data=data)
    logger.info(r.text)
    return


def send_message_with_buttions(receiver_id, message, tag_mssg, buttions):

    create_buttions = []
    for each in buttions:
        create_buttions.append({
            "content_type": "text",
            "title": each,
            "payload": tag_mssg + each
        })
    headers = {'Content-Type': 'application/json', }
    params = (('access_token', token),)

    data = json.dumps(
        {"recipient": {
            "id": receiver_id
        },
            "message": {
            "text": message,
            "quick_replies": create_buttions
        }
        })

    r = requests.post('https://graph.facebook.com/v2.10/me/messages',
                      headers=headers, params=params, data=data)
    logger.info(r.text)
    return


def name(user_id):
    params = (
        ('fields', 'first_name,last_name'),
        ('access_token', token),
    )
    r = requests.get('https://graph.facebook.com/v2.10/' +
                     user_id, params=params)
    data = json.loads(r.text)
    print(r.text)
    try:
        return data["first_name"],data["last_name"]
    except:
        return 'I','Unknown'


def SetGetStarted():
    headers = {
        'Content-Type': 'application/json',
    }
    params = (
        ('access_token', token),
    )
    data = '{ "get_started":{"payload":"GET_STARTED_PAYLOAD"}}'
    r = requests.post('https://graph.facebook.com/v2.10/me/messenger_profile',
                      headers=headers, params=params, data=data)
    print(r.text)


def mssg_read(receiver_id):
    headers = {
        'Content-Type': 'application/json',
    }
    params = (
        ('access_token', token),
    )
    data = json.dumps({
        "recipient": {"id": receiver_id},
        "sender_action": "mark_seen"
    })
    r = requests.post('https://graph.facebook.com/v2.10/me/messages',
                      headers=headers, params=params, data=data)
    return r.text


def messaging_events(payload):
    """
        check the event is a message and return the id and message
    """
    data = json.loads(payload)

    pprint.pprint(data)

    event = data["entry"][0]["messaging"]
    time = data["entry"][0]["time"]
    mssg = event[0].get("message")
    if mssg:
	if mssg.get("sticker_id"):
		return event[0]["sender"]["id"],'like',time
        attach = event[0]["message"].get('attachments')
        if not attach:
            if not event:
                return None, None, None, None
            else:
                pprint.pprint(data)
                for evt in event:
                    if "message" in evt and "text" in evt["message"]:
                        quick_reply = evt["message"].get("quick_reply")
                        if quick_reply:
                            return evt["sender"]["id"], quick_reply.get("payload"), time
                        else:
                            return evt["sender"]["id"], evt["message"]["text"].encode('unicode_escape'), time
        else:
            if attach[0].get("type") == u'image':
                return event[0]["sender"]["id"], 'thisisaimageha11o:::' + attach[0]["payload"]["url"], time
            else:
                return event[0]['sender']["id"], 'iezsxrdctfvygbhnrxdctfv', time
    else:
        return event[0]["sender"]["id"], event[0]["postback"]["payload"], time


#----------------------------------END-------------------------------------#