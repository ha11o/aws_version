from flask import Flask, request
import json
app = Flask(__name__)
app.debug = True
app.port = 8443

@app.route("/", methods=['GET'])
def main():
    return request.args.get('hub.challenge','')

if __name__ == "__main__":
    app.run()

